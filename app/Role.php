<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
        // 1 .. n SIENDO ESTO LA PARTE N
        return $this->hasMany(\App\User::class);
        // return $this->hasOne("App\Class", foreign_Key, primary_key);
        //belongsTo
        //hasMany 1 .. n --> ES LA PARTE N
        //hasOne 1 .. 1
        //belongsToMany (en los dos) n .. n
        /* La tabla de relacion N .. N la tabla se llama nombreSingularTabla_nombreSingular otra tabla
        respetando el orden alfabetico */
    }


}
