<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cathegory;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'cathegory_id'
    ];

    public function Cathegory()
    {
        $cathegory = Cathegory::findOrFail($this->cathegory_id);
        return $cathegory;
    }

    public static function FormatoFecha($date)
    {
        $date = new DateTime($date);
        return $date->format('d-m-Y');
    }
}
