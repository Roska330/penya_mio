@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')

    <h1>
        Este es el detalle del Rol <?php echo $role->id ?>
    </h1>

    <ul>
        <li>Nombre {{ $role->name }}</li>
    </ul>

    <h2>Lista de usuarios</h2>
    @foreach ($role->users as $user)
        <li>{{ $user->name }}</li>
    @endforeach
@endsection

