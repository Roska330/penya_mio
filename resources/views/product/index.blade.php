@extends('layouts.app')

@section('content')
    <h1>Lista de productos</h1>
    <a href="/products/create">Nuevo</a>
    <hr>
    <table width="100%">
        <tr><th>Nombre</th><th>Precio</th><th>Categoria</th><th>Acciones</th></tr>
        @forelse ($products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->price }}€</td>
            <td>{{ $product->Cathegory()->name }}</td>
            <td><a href="/products/{{ $product->id }}">Ver</a> | <a href="/products/{{ $product->id }}/edit">Editar</a> | <form method="post" action="/products/{{ $product->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
            </form></td>
        </tr>
    @empty
        <td>No hay productos!!</td>
    @endforelse
    </table>

    {{ $products->render() }}

@endsection
