@extends('layouts.app')

@section('content')
    <h1>Lista de usuarios</h1>
    <a href="/users/create">Nuevo</a>
    <hr>
    <table width="100%">
        <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Rol</th>
            <th>Acciones</th>
        </tr>
        @forelse ($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role->name }}</td>

            <td>
                @can("update",$user)
                    <a href="/users/{{ $user->id }}/edit">Editar</a> |
                @endcan

                <a href="/users/{{ $user->id }}">Ver</a>

                {{ csrf_field() }}

                @can("delete",$user)
                    <form method="post" action="/users/{{ $user->id }}">
                    <input type="hidden" name="_method" value="DELETE">
                    |<input type="submit" value="borrar">

                </form>
                @endcan
            </td>
        </tr>
    @empty
        <td>No hay usuarios!!</td>
    @endforelse
    </table>
    {{-- <ul>
    @forelse ($users as $user)
        <li>{{ $user->name }}: {{ $user->email }}
            <a href="/users/{{ $user->id }}/edit">Editar</a>

            <form method="post" action="/users/{{ $user->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
            </form>
        </li>
    @empty
        <li>No hay usuarios!!</li>
    @endforelse
    </ul> --}}

    {{ $users->render() }}

@endsection
