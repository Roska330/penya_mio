@extends('layouts.app')

@section('content')
    <h1>Lista de categorias</h1>
    <a href="/cathegories/create">Nuevo</a>
    <hr>
    <table width="100%">
        <tr><th>Nombre</th><th>Acciones</th></tr>
        @forelse ($cathegories as $cathegory)
        <tr>
            <td>{{ $cathegory->name }}</td>
            <td><a href="/cathegories/{{ $cathegory->id }}/edit">Editar</a> | <form method="post" action="/cathegories/{{ $cathegory->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
            </form></td>
        </tr>
    @empty
        <td>No hay categorias!!</td>
    @endforelse
    </table>

    {{ $cathegories->render() }}

@endsection
